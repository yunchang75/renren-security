package io.renren.modules.busi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.busi.entity.SProductEntity;

import java.util.Map;

/**
 * 
 *
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:04
 */
public interface SProductService extends IService<SProductEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

