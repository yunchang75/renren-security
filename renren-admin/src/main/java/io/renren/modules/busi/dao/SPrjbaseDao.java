package io.renren.modules.busi.dao;

import io.renren.modules.busi.entity.SPrjbaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:05
 */
@Mapper
public interface SPrjbaseDao extends BaseMapper<SPrjbaseEntity> {
	
}
