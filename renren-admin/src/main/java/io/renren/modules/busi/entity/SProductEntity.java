package io.renren.modules.busi.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:04
 */
@Data
@TableName("s_product")
public class SProductEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer productid;
	/**
	 * 
	 */
	private String confirmationnum;
	/**
	 * 
	 */
	private String distributionunit;
	/**
	 * 
	 */
	private String measureunit;
	/**
	 * 
	 */
	private String productbatch;
	/**
	 * 
	 */
	private String productbill;
	/**
	 * 
	 */
	private String productenterprise;
	/**
	 * 
	 */
	private String productentrytime;
	/**
	 * 
	 */
	private String productmark;
	/**
	 * 
	 */
	private String productmemo;
	/**
	 * 
	 */
	private Integer producttypeid;
	/**
	 * 
	 */
	private String productspecification;
	/**
	 * 
	 */
	private String registrationnum;
	/**
	 * 
	 */
	private String supplyprotocol;
	/**
	 * 
	 */
	private Integer usecount;
	/**
	 * 
	 */
	private String useposition;
	/**
	 * 
	 */
	private Integer prjbaseid;
	/**
	 * 
	 */
	private String productname;
	/**
	 * 
	 */
	private Integer issubmitted;
	/**
	 * 
	 */
	private String modifytime;

}
