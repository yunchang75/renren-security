package io.renren.modules.busi.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.busi.dao.SProductDao;
import io.renren.modules.busi.entity.SProductEntity;
import io.renren.modules.busi.service.SProductService;


@Service("sProductService")
public class SProductServiceImpl extends ServiceImpl<SProductDao, SProductEntity> implements SProductService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SProductEntity> page = this.page(
                new Query<SProductEntity>().getPage(params),
                new QueryWrapper<SProductEntity>()
        );

        return new PageUtils(page);
    }

}
