package io.renren.modules.busi.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.busi.entity.SProductEntity;
import io.renren.modules.busi.service.SProductService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:04
 */
@RestController
@RequestMapping("busi/sproduct")
public class SProductController {
    @Autowired
    private SProductService sProductService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("busi:sproduct:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sProductService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{productid}")
    @RequiresPermissions("busi:sproduct:info")
    public R info(@PathVariable("productid") Integer productid){
        SProductEntity sProduct = sProductService.getById(productid);

        return R.ok().put("sProduct", sProduct);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("busi:sproduct:save")
    public R save(@RequestBody SProductEntity sProduct){
        sProductService.save(sProduct);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("busi:sproduct:update")
    public R update(@RequestBody SProductEntity sProduct){
        ValidatorUtils.validateEntity(sProduct);
        sProductService.updateById(sProduct);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("busi:sproduct:delete")
    public R delete(@RequestBody Integer[] productids){
        sProductService.removeByIds(Arrays.asList(productids));

        return R.ok();
    }

}
