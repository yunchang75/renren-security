package io.renren.modules.busi.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.busi.entity.SPrjbaseEntity;
import io.renren.modules.busi.service.SPrjbaseService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:05
 */
@RestController
@RequestMapping("busi/sprjbase")
public class SPrjbaseController {
    @Autowired
    private SPrjbaseService sPrjbaseService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("busi:sprjbase:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = sPrjbaseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{prjbaseid}")
    @RequiresPermissions("busi:sprjbase:info")
    public R info(@PathVariable("prjbaseid") Integer prjbaseid){
        SPrjbaseEntity sPrjbase = sPrjbaseService.getById(prjbaseid);

        return R.ok().put("sPrjbase", sPrjbase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("busi:sprjbase:save")
    public R save(@RequestBody SPrjbaseEntity sPrjbase){
        sPrjbaseService.save(sPrjbase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("busi:sprjbase:update")
    public R update(@RequestBody SPrjbaseEntity sPrjbase){
        ValidatorUtils.validateEntity(sPrjbase);
        sPrjbaseService.updateById(sPrjbase);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("busi:sprjbase:delete")
    public R delete(@RequestBody Integer[] prjbaseids){
        sPrjbaseService.removeByIds(Arrays.asList(prjbaseids));

        return R.ok();
    }

}
