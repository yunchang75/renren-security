package io.renren.modules.busi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.busi.entity.SPrjbaseEntity;

import java.util.Map;

/**
 * 
 *
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:05
 */
public interface SPrjbaseService extends IService<SPrjbaseEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

