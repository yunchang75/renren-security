package io.renren.modules.busi.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:05
 */
@Data
@TableName("s_prjbase")
public class SPrjbaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer prjbaseid;
	/**
	 * 
	 */
	private String buildunit;
	/**
	 * 
	 */
	private String buildunitcontacts;
	/**
	 * 
	 */
	private String buildunittel;
	/**
	 * 
	 */
	private Float constructionarea;
	/**
	 * 
	 */
	private String constructionunit;
	/**
	 * 
	 */
	private String constunitcontacts;
	/**
	 * 
	 */
	private String constunittel;
	/**
	 * 
	 */
	private Date fillformtime;
	/**
	 * 
	 */
	private String organizationcode;
	/**
	 * 
	 */
	private String prjaddress;
	/**
	 * 
	 */
	private String prjmemo;
	/**
	 * 
	 */
	private String prjname;
	/**
	 * 
	 */
	private String prjnature;
	/**
	 * 
	 */
	private String structureform;
	/**
	 * 
	 */
	private String superunitcontacts;
	/**
	 * 
	 */
	private String superunittel;
	/**
	 * 
	 */
	private String supervisoryunit;

}
