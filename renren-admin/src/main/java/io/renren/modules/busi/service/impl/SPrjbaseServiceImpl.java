package io.renren.modules.busi.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.busi.dao.SPrjbaseDao;
import io.renren.modules.busi.entity.SPrjbaseEntity;
import io.renren.modules.busi.service.SPrjbaseService;


@Service("sPrjbaseService")
public class SPrjbaseServiceImpl extends ServiceImpl<SPrjbaseDao, SPrjbaseEntity> implements SPrjbaseService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SPrjbaseEntity> page = this.page(
                new Query<SPrjbaseEntity>().getPage(params),
                new QueryWrapper<SPrjbaseEntity>()
        );

        return new PageUtils(page);
    }

}
