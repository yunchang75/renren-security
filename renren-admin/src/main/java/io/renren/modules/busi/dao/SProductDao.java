package io.renren.modules.busi.dao;

import io.renren.modules.busi.entity.SProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author liu Yc
 * @email yunchang75@aliyun.com
 * @date 2020-08-07 10:39:04
 */
@Mapper
public interface SProductDao extends BaseMapper<SProductEntity> {
	
}
