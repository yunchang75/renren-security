$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'busi/sprjbase/list',
        datatype: "json",
        colModel: [			
			{ label: 'prjbaseid', name: 'prjbaseid', index: 'prjbaseid', width: 50, key: true },
			{ label: '', name: 'buildunit', index: 'buildunit', width: 80 }, 			
			{ label: '', name: 'buildunitcontacts', index: 'buildunitcontacts', width: 80 }, 			
			{ label: '', name: 'buildunittel', index: 'buildunittel', width: 80 }, 			
			{ label: '', name: 'constructionarea', index: 'constructionarea', width: 80 }, 			
			{ label: '', name: 'constructionunit', index: 'constructionunit', width: 80 }, 			
			{ label: '', name: 'constunitcontacts', index: 'constunitcontacts', width: 80 }, 			
			{ label: '', name: 'constunittel', index: 'constunittel', width: 80 }, 			
			{ label: '', name: 'fillformtime', index: 'fillformtime', width: 80 }, 			
			{ label: '', name: 'organizationcode', index: 'organizationcode', width: 80 }, 			
			{ label: '', name: 'prjaddress', index: 'prjaddress', width: 80 }, 			
			{ label: '', name: 'prjmemo', index: 'prjmemo', width: 80 }, 			
			{ label: '', name: 'prjname', index: 'prjname', width: 80 }, 			
			{ label: '', name: 'prjnature', index: 'prjnature', width: 80 }, 			
			{ label: '', name: 'structureform', index: 'structureform', width: 80 }, 			
			{ label: '', name: 'superunitcontacts', index: 'superunitcontacts', width: 80 }, 			
			{ label: '', name: 'superunittel', index: 'superunittel', width: 80 }, 			
			{ label: '', name: 'supervisoryunit', index: 'supervisoryunit', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sPrjbase: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.sPrjbase = {};
		},
		update: function (event) {
			var prjbaseid = getSelectedRow();
			if(prjbaseid == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(prjbaseid)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.sPrjbase.prjbaseid == null ? "busi/sprjbase/save" : "busi/sprjbase/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.sPrjbase),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var prjbaseids = getSelectedRows();
			if(prjbaseids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "busi/sprjbase/delete",
                        contentType: "application/json",
                        data: JSON.stringify(prjbaseids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(prjbaseid){
			$.get(baseURL + "busi/sprjbase/info/"+prjbaseid, function(r){
                vm.sPrjbase = r.sPrjbase;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});