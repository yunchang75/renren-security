$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'busi/sproduct/list',
        datatype: "json",
        colModel: [			
			{ label: 'productid', name: 'productid', index: 'productid', width: 50, key: true },
			{ label: '', name: 'confirmationnum', index: 'confirmationnum', width: 80 }, 			
			{ label: '', name: 'distributionunit', index: 'distributionunit', width: 80 }, 			
			{ label: '', name: 'measureunit', index: 'measureunit', width: 80 }, 			
			{ label: '', name: 'productbatch', index: 'productbatch', width: 80 }, 			
			{ label: '', name: 'productbill', index: 'productbill', width: 80 }, 			
			{ label: '', name: 'productenterprise', index: 'productenterprise', width: 80 }, 			
			{ label: '', name: 'productentrytime', index: 'productentrytime', width: 80 }, 			
			{ label: '', name: 'productmark', index: 'productmark', width: 80 }, 			
			{ label: '', name: 'productmemo', index: 'productmemo', width: 80 }, 			
			{ label: '', name: 'producttypeid', index: 'producttypeid', width: 80 }, 			
			{ label: '', name: 'productspecification', index: 'productspecification', width: 80 }, 			
			{ label: '', name: 'registrationnum', index: 'registrationnum', width: 80 }, 			
			{ label: '', name: 'supplyprotocol', index: 'supplyprotocol', width: 80 }, 			
			{ label: '', name: 'usecount', index: 'usecount', width: 80 }, 			
			{ label: '', name: 'useposition', index: 'useposition', width: 80 }, 			
			{ label: '', name: 'prjbaseid', index: 'prjbaseid', width: 80 }, 			
			{ label: '', name: 'productname', index: 'productname', width: 80 }, 			
			{ label: '', name: 'issubmitted', index: 'issubmitted', width: 80 }, 			
			{ label: '', name: 'modifytime', index: 'modifytime', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		sProduct: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.sProduct = {};
		},
		update: function (event) {
			var productid = getSelectedRow();
			if(productid == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(productid)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.sProduct.productid == null ? "busi/sproduct/save" : "busi/sproduct/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.sProduct),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var productids = getSelectedRows();
			if(productids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "busi/sproduct/delete",
                        contentType: "application/json",
                        data: JSON.stringify(productids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(productid){
			$.get(baseURL + "busi/sproduct/info/"+productid, function(r){
                vm.sProduct = r.sProduct;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});